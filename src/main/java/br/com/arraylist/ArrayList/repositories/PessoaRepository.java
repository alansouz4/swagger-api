package br.com.arraylist.ArrayList.repositories;

import br.com.arraylist.ArrayList.models.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {
}
