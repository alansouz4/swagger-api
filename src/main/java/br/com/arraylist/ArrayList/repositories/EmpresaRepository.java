package br.com.arraylist.ArrayList.repositories;

import br.com.arraylist.ArrayList.models.Empresa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpresaRepository extends JpaRepository<Empresa, Integer> {
}
