package br.com.arraylist.ArrayList.mappers;


import br.com.arraylist.ArrayList.DTO.CreateEmpresaPostRequest;
import br.com.arraylist.ArrayList.models.Empresa;

public class MapperCreateEmpresaPostRequest {

    public static Empresa converteParaEmpresa(CreateEmpresaPostRequest createEmpresaPostRequest){

        Empresa empresa = new Empresa();

        empresa.setNome(createEmpresaPostRequest.getNome());
        empresa.setPessoas(createEmpresaPostRequest.getPessoas());

        return empresa;
    }
}
