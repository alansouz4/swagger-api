package br.com.arraylist.ArrayList.mappers;


import br.com.arraylist.ArrayList.DTO.CreateEmpresaPostResponse;
import br.com.arraylist.ArrayList.models.Empresa;

public class MapperCreateEmpresaPostResponse {

    public static CreateEmpresaPostResponse converteParaCreateEmpresaPostResponse(Empresa empresa) {

        CreateEmpresaPostResponse createEmpresaPostResponse = new CreateEmpresaPostResponse();

        createEmpresaPostResponse.setId(empresa.getId());
        createEmpresaPostResponse.setNome(empresa.getNome());
        createEmpresaPostResponse.setPessoas(empresa.getPessoas());

        return createEmpresaPostResponse;
    }

}
