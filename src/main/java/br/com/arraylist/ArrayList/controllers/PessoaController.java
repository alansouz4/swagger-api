package br.com.arraylist.ArrayList.controllers;

import br.com.arraylist.ArrayList.models.Pessoa;
import br.com.arraylist.ArrayList.services.PessoaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value="API REST Cadastro Pessoa")
@RequestMapping(value = "/pessoas")
@RestController
public class PessoaController {

//    Logger logger = LoggerFactory.getLogger(PessoaController.class);

    @Autowired
    private PessoaService service;

    @ApiOperation(value = "Cria cadastro pessoa")
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Pessoa salvar(@RequestBody @Validated Pessoa pessoa){
//        logger.error("Erro");
        return service.create(pessoa);
    }

    @ApiOperation(value = "Obtem clientes")
    @GetMapping
    public Iterable<Pessoa> get() {
        return service.get();
    }

    @ApiOperation(value = "Obtem cliente por id")
    @GetMapping("/{id}")
    public List<Pessoa> getById(@PathVariable List<Integer> id) {
        return service.getById(id);
    }
}
