package br.com.arraylist.ArrayList.controllers;

import br.com.arraylist.ArrayList.DTO.CreateEmpresaPostRequest;
import br.com.arraylist.ArrayList.DTO.CreateEmpresaPostResponse;
import br.com.arraylist.ArrayList.mappers.MapperCreateEmpresaPostRequest;
import br.com.arraylist.ArrayList.mappers.MapperCreateEmpresaPostResponse;
import br.com.arraylist.ArrayList.models.Empresa;
import br.com.arraylist.ArrayList.services.EmpresaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(value="API REST Cadastro Empresas")
@RestController
@RequestMapping(value = "/empresas")
public class EmpresaController {

    @Autowired
    private EmpresaService service;

    @ApiOperation(value = "Cria cadastro empresa")
    @PostMapping
    public ResponseEntity<CreateEmpresaPostResponse> salvar(@RequestBody @Validated CreateEmpresaPostRequest empresaPostRequest)  {

        Empresa empresa = MapperCreateEmpresaPostRequest.converteParaEmpresa(empresaPostRequest);
        Empresa empresaObj = service.create(empresa);

        CreateEmpresaPostResponse createEmpresaPostResponse =
                MapperCreateEmpresaPostResponse.converteParaCreateEmpresaPostResponse(empresaObj);

        return ResponseEntity.status(201).body(createEmpresaPostResponse);
    }

    @ApiOperation(value = "Obtem empresas")
    @GetMapping
    public Iterable<Empresa> get() {
        return service.get();
    }

    @ApiOperation(value = "Obtem empresa por id")
    @GetMapping(value = "/{id}")
    public Empresa getById(@PathVariable int id) {
        return service.getById(id);
    }
}
