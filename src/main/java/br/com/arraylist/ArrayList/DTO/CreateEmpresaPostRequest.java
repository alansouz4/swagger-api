package br.com.arraylist.ArrayList.DTO;

import br.com.arraylist.ArrayList.models.Pessoa;

import java.util.List;


public class CreateEmpresaPostRequest {

    private String nome;
    private List<Pessoa> pessoas;

    public CreateEmpresaPostRequest() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }
}
