package br.com.arraylist.ArrayList.services;

import br.com.arraylist.ArrayList.models.Empresa;
import br.com.arraylist.ArrayList.models.Pessoa;
import br.com.arraylist.ArrayList.repositories.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PessoaService {

    @Autowired
    private PessoaRepository repository;

    @Autowired
    private EmpresaService empresaService;

    public Pessoa create(Pessoa pessoa){
        return repository.save(pessoa);
    }

    public List<Pessoa> getById(List<Integer> id){
        Iterable<Pessoa> pessoa = repository.findAllById(id);
        return (List) pessoa;
    }

    public Iterable<Pessoa> get(){
        return repository.findAll();
    }
}
