package br.com.arraylist.ArrayList.services;

import br.com.arraylist.ArrayList.models.Empresa;
import br.com.arraylist.ArrayList.models.Pessoa;
import br.com.arraylist.ArrayList.repositories.EmpresaRepository;
import br.com.arraylist.ArrayList.repositories.LogRepository;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

@Service
public class EmpresaService {

    @Autowired
    private EmpresaRepository repository;

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private LogRepository logRepository;

    private static final String METODO_POST = "[POST] ";
    private static final String METODO_GET = "[GET] ";

    public Empresa create(Empresa empresa)  {

        List<Integer> empresaId = new ArrayList<>();
        for (Pessoa pessoa: empresa.getPessoas()){
            int id = pessoa.getId();
            empresaId.add(id);
        }

        List<Pessoa> pessoas = pessoaService.getById(empresaId);
        empresa.setPessoas(pessoas);

        Empresa empresaObj = repository.save(empresa);

        logarEmpresa(empresaObj, METODO_POST);

        return empresaObj;
    }

    public Empresa getById(int id){
        Optional<Empresa> empresaOptional = repository.findById(id);

        if (empresaOptional.isPresent()){
            logarEmpresa(empresaOptional.get(), METODO_GET);
            return empresaOptional.get();
        }
        logarErro("Empresa não encontrada. Id = " + id, METODO_GET);
        throw new RuntimeException("Empresa não encontrada.");
    }

    public Iterable<Empresa> get(){
        return repository.findAll();
    }

    public void logarEmpresa(Empresa empresa, String method) {
        logRepository.log(Level.INFO, this.getClass(),
                (method + "Nome: " + empresa.getNome()));
    }

    public void logarErro(String mensagem, String method) {
        logRepository.log(Level.INFO, this.getClass(), method + mensagem);
    }

}
